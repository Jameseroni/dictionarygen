Inside the data folder you will find several files containing text.

Imagine these are PCS freetext responses filled by EMTs. EMTs use a certain 
subset of language that most people don't. This includes medical terms and medication. This is why we have 
grammar files that tell the software what words to look for. If we include
the english dictionary, we might find the word 'aristocrat' in our grammar, which
isn't likely to be needed on a PCS, and the computer may waste time trying to match against that word.

We would like you to parse these files and generate a precursor to our PCS 'dictionary'.

-- Output to a file a list of unique words in order of highest occurence. No duplicate words. 
-- Measure how long this process takes programmatically, and how many of each word you find.

This should act on all files in the data directory, not just one.
Ignore special characters and punctuation.
You may use C#, Python, or Javascript.

Keep in mind that our next steps will include selecting our fastest accurate solution and
connecting it to a Database to run on 20 years worth of real data. (Okay, maybe less at first.) 

This way we don't need to have our speech recognition 
looking at the entire english dictionary.